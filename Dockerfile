# Utiliza una imagen base de Python
FROM python:3.11

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia los archivos de configuración de Poetry al contenedor
COPY pyproject.toml poetry.lock /app/

# Instala Poetry
RUN pip install poetry

# Instala las dependencias del proyecto
RUN poetry install --no-root --no-dev

# Copia el código fuente de la aplicación al contenedor
COPY . /app

#RUN poetry env use

# Establece las variables de entorno
ENV PYTHONUNBUFFERED 1

# Expone el puerto en el que se ejecuta la aplicación (si es necesario)
EXPOSE 3011

# Comando para ejecutar la aplicación
CMD ["poetry", "run", "uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "3011"]
