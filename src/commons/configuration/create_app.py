import graphene
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from starlette_graphene3 import GraphQLApp

from src.posts.controllers.mutations import CreatePost
from src.posts.controllers.queries import PostQuery
from src.commons.configuration.database import Base, engine
from dotenv import load_dotenv
from src.routes.api import router as post_router
import os

load_dotenv()


class Query(PostQuery):
    pass


class Mutation(graphene.ObjectType):
    create_post = CreatePost.Field()


schemas = graphene.Schema(query=Query, mutation=Mutation)


def create_app():
    global app
    app = FastAPI()
    origins = [f'http://localhost:{os.getenv("PORT")}']
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.add_route("/graphql", GraphQLApp(schema=schemas))
    app.include_router(post_router)
    Base.metadata.create_all(bind=engine)
    return app
