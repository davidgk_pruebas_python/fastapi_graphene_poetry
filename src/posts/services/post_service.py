from fastapi import HTTPException
from ..repository import post_repository as repository


def save_post( db, post_dict):
    try:
        # posts.append(post_dict)
        return repository.create_post(db, post_dict)
    except Exception as ex:
        raise HTTPException(status_code=500,detail=ex)


async def find_post_by_id( id, db):
    try:
        return repository.get_post_by_id(db, id)
    except Exception as ex:
        raise HTTPException(status_code=404,detail=ex)


def get_posts(db):
    return repository.get_posts(db)


async def find_and_update(post_id, post_data, db ):
    try:
        post_db = await repository.get_post_by_id(db, post_id)
        if not post_db:
            raise HTTPException(status_code=404, detail="Post not found")
        return repository.update(db, post_db, post_data )
    except BaseException as exc:
        print(exc)
        return None


def evaluate_post(post, post_id: str, title=None):
    exists = post["id"] == post_id
    if exists and title:
        exists = post["title"] == title
    return exists


async def delete_post(post_id, db):
    return repository.delete(db, post_id)
