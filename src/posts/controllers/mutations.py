import graphene
from ..controllers.post_controller import save_posts
from src.commons.configuration.database import SessionLocal
from ..models.post import PostType


def get_values_for_creation(title, author, content, published):
    return PostType(title=title,
                    author=author,
                    content=content if content else 'None Content',
                    published=published if published else False, )


class CreatePost(graphene.Mutation):

    success = graphene.Boolean()
    message = graphene.String()
    post_id = graphene.String()
    class Arguments:
        title = graphene.String(required=True)
        author = graphene.String(required=True)
        content = graphene.String(required=False)
        published = graphene.Boolean(required=False)


    async def mutate(self, info, title, author, content, published):
        try:
            db = SessionLocal()
            values = get_values_for_creation(title, author, content, published)
            post = await save_posts(values.__dict__, db)
            success = True
            message = "Post created successfully"
            post_id = post.id
        except Exception as ex:
            success = False
            message = f"User cannot be created - {str(ex)}"
        return CreatePost(success=success, message=message, post_id=post_id)


