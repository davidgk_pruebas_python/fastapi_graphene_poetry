import graphene

from src.commons.configuration.database import SessionLocal
from src.posts.controllers import post_controller as controller
from src.posts.models.post import PostType


class PostQuery(PostType):
    hello = graphene.String()
    post = graphene.Field(PostType, post_id=graphene.String())
    posts = graphene.List(PostType)

    def resolve_hello(self, info):
        return "Hola, mundo!"

    async def resolve_posts(self, info) :
        db = SessionLocal()
        return await controller.get_posts(db)

    async def resolve_post(self, info, post_id: graphene.String()) :
        db = SessionLocal()
        if post_id is not None:
            return await controller.get_post_by_id(post_id, db)

