from datetime import datetime

import graphene
from sqlalchemy import Boolean, Column, String, DateTime

from src.commons.configuration.database import Base


class PostDbModel(Base):
    __tablename__ = "posts"

    id = Column(String, primary_key=True, index=True)
    title = Column(String, index=False, nullable= True)
    author = Column(String, index=True, nullable= True)
    content = Column(String, nullable= True)
    published_at = Column(DateTime, nullable= True)
    created_at = Column(DateTime,default= datetime.now())
    published = Column(Boolean, default=False)


class PostType(graphene.ObjectType):
    id = graphene.String()
    title = graphene.String()
    author = graphene.String()
    content = graphene.String()
    created_at = graphene.DateTime()
    published_at = graphene.DateTime()
    published = graphene.Boolean()


