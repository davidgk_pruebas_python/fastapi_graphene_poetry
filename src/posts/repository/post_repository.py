from uuid import uuid4 as uuid
from sqlalchemy.orm import Session
from ..models.post import PostDbModel


def get_post(db: Session, post_id: str):
    return db.query(PostDbModel).filter(PostDbModel.id == post_id).first()


def get_post_by_id(db: Session, id: str):
    return db.query(PostDbModel).filter_by(id=id).first()


def get_posts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(PostDbModel).offset(skip).limit(limit).all()


def create_post(db: Session, post: dict):
    id=uuid().__str__()
    db_post = PostDbModel(id=id,
                        author=post["author"],
                        title=post["title"],
                        content=post["content"],
                        published_at=None,
                        )
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


def update(db, post_db, post_data):
    for key, value in post_data.items():
        setattr(post_db, key, value)
    db.add(post_db)
    db.commit()
    db.refresh(post_db)
    return post_db

def delete(db, post_id):
    db.query(PostDbModel).filter_by(id=post_id).delete()
    db.commit()
