# Example FastAPI; GraphQL(through graphene) and Poetry


## Install first
On project path , run `poetry install`

# How to run 

1. Import from developers the collection into postman
2. run `docker-compose up -d --no-recreate app`

# About the app

### We want to create posts about books so if we can create one:
* With graphql:
````
post to http://0.0.0.0:3011/graphql

mutation {
      createPost(title: "Clean Ultra rapid Code", author: "Robert C. Martin", content: "Short methods" , published: true ) {
          message
          success
          postId
      }
  } 

````
* with REST
```
post to http://0.0.0.0:3011/api/posts
# Body:
{
    "author": "J.K Rowling",
    "title": "Harry Potter",
    "content": "A magic book"
}


```

### We want to get the posts :
* With graphql:
````
post to http://0.0.0.0:3011/graphql

{
  post (postId:<the id you get in creation>) {
      author
      title
  }
}
````
* with REST
```
GET to http://0.0.0.0:3011/api/posts/<the id you get in creation>

```
